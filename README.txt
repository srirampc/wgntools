wgntools - Set of tools to analyze Whole Genome Networks:
=========================================================

This package provides utility functions to enable the following
analyses using whole genome networks.

(A) MRA, overlap and GSEA1 analyses using RTN package
(B) NEST score analysis

(A) Using RTN library with Whole Genome Networks :
==================================================

1. Install the following R libraries : RTN, stringr, igraph, dplyr

2. To run the A. thaliana examples,
   also install wgn.athaliana package from bitbucket:

> devtools::install_bitbucket("srirampc/wgn.athaliana")

3. Install wgntools from bitbucket repo:

> devtools::install_bitbucket("srirampc/wgntools")

4. Load the network corresponding to transcription factors. There are two ways
   to

   a. Get the subnetwork corresponding the input transcription factors from
      whole genome network.

   > atfnet = wgntools::load.tf.sub.network(wgn.athaliana::btf300,
                                  wgn.athaliana::development,
                                  wgn.athaliana::symbol2probe)
      The function takes three arguments:
      (i) The transcription factors are provided as a dataframe
          with a single column with name "SYMBOL". In the example above, we use the
          wgn.athaliana::btf300 list available in the wgn.athaliana package.

      (ii) The second argument indicates what whole genome network should be used for
           analysis. The network is a data frome with three columns : "s", "t" and "wt",
           with each row representing a unique edge in the network.Here,
           we use the "development" network available with the "wgn.athaliana" package.

      (iii) The third argument is the annotation data frame with two columns "SYMBOL" and
           "PROBEID" - mapping the gene identifier to the probe.

   b. First we load the annotation data using "load.symbol.annotation" function, which
      takes in the annotation info as a CSV file with two columns
      "SYMBOL" and "PROBEID" : gene symbol on the first column, and the corresponding
      probes in the second column.

   > annot.df = load.symbol.annotation("annotation.csv")

      Then, get the subnetwork corresponding the input transcription factors from any
      network.

   > atfnet = load.tf.network.from("arabidopsis-tfs.csv", "hormone-network1-v1.csv",
                                    annot.df)
      The function takes three arguments:
      (i) The transcription factors are provided as a csv file
          with a single column with name "SYMBOL".
      (ii) The second argument indicates what network should be used for analysis,
           provided as a CSV file with three columns : "s", "t" and "wt". Each row
           is an edge in the network.
      (iii) The third argument is the annotation data frame loaded in the previous
            step.

4. Compute phenotype values as the fold change of differential expression.

   > phenotype = wgntools::compute.diff.expr.df(wgn.athaliana::expr.WT,
                                      wgn.athaliana::expr.WTBL,
                                      wgn.athaliana::probe2symbol,
                                      threshold = 1.0)

   In the above example, we use data from "wgn.athaliana" package. The
   two conditions are "WT" (wild type) and "WT+BL" (wild type with BL mutation).
   The datasets wgn.athaliana::expr.WT and wgn.athaliana::expr.WTBL have the
   average expression values for WT and WT+BL conditions respectively. The threshold
   argument is used to select the hits.

5. Construct the RTNA object from the network, the phenotype values and the
   hits.

   > rtna = wgntools::tfnet2tna(atfnet, phenotype$pheno, phenotype$hits,
                      wgn.athaliana::probe2symbol)

   Phenotype values here is the differential expression values of all the
   probes. Hits are the probes showing significant expression value. The last
   argumenet is the PROBEID, SYMBOL annotation dataframe. Here, we demonstrate it
   using the wgn.athaliana::probe2symbol.

6. rtna object can be used for further analysis - similarto Section 2.2 in the RTN
   manual. The MRA analysis, overlap analysis and GSEA1 analysis.

   > rtna = tna.mra(rtna)

   > rtna = tna.overlap(rtna)

   > rtna = tna.gsea1(rtna)


(B) PCC for Regulon from Network Structure :
============================================
Given a list of TFs and a network, the Pearson Correlation Coefficient (PCC)
between two regulons is computed from the network as follows:

    (i) For every pair of TFs, do steps 2-4  below.

    (ii) Find all the common targets of TF1 and TF2 in the network. Suppose
    there are "n" number of common targets between TF1 and TF2.

    (iii) For each shared target, find the weight values between the transcription
    factor and the target. Here, we have now two weight values for each target
    - one for TF1 and one for TF2. In total, there are "n" values
    corresponding to TF1 and "n" values corresponding to TF2.

    (iv) Compute PCC between the "n" weight values for TF1 and "n" values for TF2.

The weight could be computed by any means say Mutual Information or PCC itself.

The "regulon.correlation" function can be used to compute as below

  > tf.nbx = regulon.correlation(wgn.athaliana::btf300,
                                 wgn.athaliana::development,
                                 wgn.athaliana::symbol2probe)

The function takes three arguments:
  (i) The transcription factors are provided as a dataframe
      with a single column with name "SYMBOL". In the example above, we use the
      wgn.athaliana::btf300 list available in the wgn.athaliana package.

  (ii) The second argument indicates what whole genome network should be used for
       analysis. The network is a data frome with three columns : "s", "t" and "wt",
       with each row representing a unique edge in the network.Here,
       we use the "development" network available with the "wgn.athaliana" package.

  (iii) The third argument is the annotation data frame with two columns "SYMBOL" and
       "PROBEID" - mapping the gene identifier to the probe.

The function returns a data frame with four columns, described as below
   (a) "R1", "R2" corresponding to a pair of TFs with have atleast two or more shared targets.
   (b) "n" number of shared targets.
   (c) "pcc"  : PCC values between TF1 and TF2 computed as described above.


(C) NEST score analysis with Whole Genome Networks :
====================================================

To compute NEST score, use the nest.subnet function as follows:

> nx = nest.subnet(wgn.athaliana::full.union, wgn.athaliana::btf300,
                   wgn.athaliana::expr.WT, wgn.athaliana::probe2symbol)

The function has four necessary arguments and a fifth optional argument:
(i) The first argument is the whole genome network, with each row being
    the edge, and has the columns "s", "t", "wt" : "wt" column has the
    weights assigned to the edge.
(ii) The second argument is a list of transcription factors: a data frame
     with single column "SYMBOL"
(iii) The third argument is a weight data frame. It should have two columns
      "PROBEID" and "VALUE". Each row lists the weight corresponding to
      a given probe. When the weight data frame is expression value as given
      above, then it computes the NEST score.
(iv) Annotation data frame with two columns "SYMBOL" and
     "PROBEID" - mapping the gene identifier to the probe.
(v) Optional argument "subnet" if given a list of gene identifers, extracts the
    subnetwork of these genes before computing the NEST value.
The output is a data frame with four columns : NEST, SYMBOL, PROBEID, DEGREE

If the NEST should be computed only for the subnetwork involving a set of
input ATHIDS then the fifth argument should be set to input list as below:

> nx = nest.subnet(wgn.athaliana::full.union, wgn.athaliana::btf300,
                   wgn.athaliana::expr.WT, wgn.athaliana::probe2symbol,
                   subnet=wgn.athaliana::btf300)

Here, both the subnetwork gene list and the TF list are the same. They can
any two different set of genes.

In order to extract a subnetwork of genes from a whole network, call the
function wgntools::network.subnet with three arguments (a) source network
(b) probe mapping and (c) list of genes in a data frame with a single column
"SYMBOL".

> snt = wgntools::network.subnet(wgn.athaliana::full.union,
                                 wgn.athaliana::probe2symbol,
                                 wgn.athaliana::btf300)

It returns a data frame listing all the edges of the subnetwork and the edge weights.

